package red.singular.bloggy.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lombok.extern.slf4j.Slf4j;
import red.singular.bloggy.util.TokenHelper;

/**
 * Check if a CSRF Token is stored in session and if it matches
 */
@Slf4j
@WebFilter(filterName="csrf-token-filter")
public class CsrfTokenFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void destroy() {}

    private void rejectSubmission(
        HttpServletRequest httpRequest,
        HttpServletResponse httpResponse,
        String reason
    ) throws IOException, ServletException {

        httpResponse.sendError(403, reason);

    }

    @Override
    public void doFilter(
        ServletRequest request,
        ServletResponse response,
        FilterChain chain
    ) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession();
        String requiredToken = (String) session.getAttribute(
                TokenHelper.CSRF_TOKEN_VALUE_NAME);
        String submittedToken = httpRequest.getParameter(
                TokenHelper.CSRF_TOKEN_VALUE_NAME);

        if (requiredToken != null) {
            if (submittedToken == null) {
                log.info(
                    "A CSRF Token was required in the session,"
                    + " but none was submitted"
                );
                this.rejectSubmission(httpRequest, httpResponse,
                    "No  value for _csrfToken");
            }
            else if (!requiredToken.equals(submittedToken)) {
                log.info(
                    "The CSRF Token given does not match the one expected"
                    + "in the session (given: <{}>, expected: <{}>",
                    submittedToken,
                    requiredToken
                );
                this.rejectSubmission(httpRequest, httpResponse,
                    "No valid value for _csrfToken");
            }
            else {
                log.trace("Valid CSRF Token recieved");
                chain.doFilter(request, response);
            }
        }
        else {
            log.debug("No value for CSRF token in the session");

            if (submittedToken != null) {
                log.warn(
                    "A value was submitted for {} ({}) but none was"
                    + " stored in the session",
                    TokenHelper.CSRF_TOKEN_VALUE_NAME,
                    submittedToken
                );
            }

            chain.doFilter(request, response);
        }
    }
}
