package red.singular.bloggy.models;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class Article {
    private long id;
    private String title;
    private String body;
    private LocalDateTime created;
}
