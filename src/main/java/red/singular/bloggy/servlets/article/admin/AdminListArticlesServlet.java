package red.singular.bloggy.servlets.article.admin;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import red.singular.bloggy.models.Article;

@WebServlet("/article/list")
public class AdminListArticlesServlet extends HttpServlet {
    private static final long serialVersionUID = -7923393155595105524L;

    @Resource
    DataSource datasource;

    protected List<Article> findAllArticles() throws SQLException {
        List<Article> articles = new ArrayList<>();
        String query = "SELECT id, title, body, created FROM article ORDER BY created DESC";

        try (
            Connection connection = datasource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet results = statement.executeQuery();
        ) {
            while (results.next()) {
                Article article = new Article();
                article.setId(results.getInt(1));
                article.setTitle(results.getString(2));
                article.setBody(results.getString(3));
                article.setCreated(results.getTimestamp(4).toLocalDateTime());
                articles.add(article);
            }
        }

        return articles;
    }


    @Override
    public void init() throws ServletException {
        try {
            if (datasource == null) {
                Context initCtx = new InitialContext();
                Context envCtx = (Context) initCtx.lookup("java:comp/env");
                this.datasource = (DataSource) envCtx.lookup("jdbc/bloggy");
            }
        }
        catch (NamingException ne) {
            throw new ServletException(ne);
        }
    }

    @Override
    protected void doGet(
        HttpServletRequest req,
        HttpServletResponse resp
    ) throws ServletException, IOException {
        try {
            req.setAttribute("articles", this.findAllArticles());
        }
        catch (SQLException sqle) {
            throw new ServletException(sqle);
        }

        req.getRequestDispatcher("/WEB-INF/jsp/pages/listArticles.jsp")
            .forward(req, resp);
    }
}
