package red.singular.bloggy.servlets.article;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import lombok.extern.slf4j.Slf4j;
import red.singular.bloggy.dao.ArticleDAO;
import red.singular.bloggy.util.DbHelper;

@Slf4j
@WebServlet("")
public class RecentArticlesServlet extends HttpServlet {
    private static final long serialVersionUID = -3040810275183473611L;
    private static final int ITEMS_PER_PAGE = 8;

    @Resource
    DataSource datasource;

    ArticleDAO articleDAO;

    @Override
    public void init() throws ServletException {
        try {
            if (datasource == null) {
                this.datasource = DbHelper.getDataSource();
            }
        }
        catch (NamingException ne) {
            throw new ServletException(ne);
        }

        this.articleDAO = new ArticleDAO(this.datasource);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException
    {
        int page;

        try {
            page = req.getParameter("page") != null
                ? Integer.parseInt(req.getParameter("page"))
                : 1;
            if (page < 0) { page = 1; }
        }
        catch (NumberFormatException nfe) {
            page = 1;
        }

        log.debug("Display the page {} of articles", page);

        try {
            req.setAttribute("articleCount", this.articleDAO.getCount());
            req.setAttribute("itemsPerPage", ITEMS_PER_PAGE);
            req.setAttribute("currentPage", page);
            req.setAttribute("articles", this.articleDAO.findAll(page, ITEMS_PER_PAGE));
        }
        catch (SQLException sqle) {
            throw new ServletException(sqle);
        }

        req.getRequestDispatcher("/WEB-INF/jsp/pages/home.jsp").forward(req, resp);
    }
}
