package red.singular.bloggy.servlets.article;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import lombok.extern.slf4j.Slf4j;
import red.singular.bloggy.models.Article;
import red.singular.bloggy.dao.ArticleDAO;
import red.singular.bloggy.util.DbHelper;

@Slf4j
@WebServlet("/article/view")
public class ViewArticleServlet extends HttpServlet {
    private static final long serialVersionUID = -3040810275183473611L;

    @Resource
    DataSource datasource;

    ArticleDAO articleDAO;

    @Override
    public void init() throws ServletException {
        try {
            if (datasource == null) {
                this.datasource = DbHelper.getDataSource();
            }
        }
        catch (NamingException ne) {
            throw new ServletException(ne);
        }

        this.articleDAO = new ArticleDAO(this.datasource);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException
    {
        long id = 0;
        boolean isValidId = true;
        Article article = null;

        log.debug("Show article {}", article);

        if (req.getParameter("id") != null) {
            try {
                id = Long.parseLong(req.getParameter("id"));
            }
            catch (NumberFormatException nfe) {
                isValidId = false;
            }
        }
        else {
            isValidId = false;
        }

        try {
            article = this.articleDAO.findById(id);
        }
        catch (SQLException sqle) {
            throw new ServletException(sqle);
        }

        if (isValidId && article != null) {
            req.setAttribute("article", article);
            req.getRequestDispatcher("/WEB-INF/jsp/pages/viewArticle.jsp")
                .forward(req, resp);
        }
        else {
            resp.sendError(404);
        }
    }
}
