package red.singular.bloggy.util;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public final class FlashHelper {
    private FlashHelper() {
        throw new UnsupportedOperationException(
            "This class is not meant to be instanciated");
    }

    static public void addMessage(
        FlashMessage.Level level,
        String message,
        HttpServletRequest req
    ) {
        if (req.getSession().getAttribute("flashBag") == null) {
            req.getSession().setAttribute("flashBag", new ArrayList<FlashMessage>());
        }

        @SuppressWarnings("unchecked")
        List<FlashMessage> flashBag
            = (List<FlashMessage>) req
                .getSession()
                .getAttribute("flashBag");

        flashBag.add(new FlashMessage(level, message));
    }

    static public void addMessage(
        String message,
        HttpServletRequest req
    ) {
        addMessage(FlashMessage.Level.INFO, message, req);
    }

    static public void addInfo(
        String message,
        HttpServletRequest req
    ) {
        addMessage(FlashMessage.Level.INFO, message, req);
    }

    static public void addError(
        String message,
        HttpServletRequest req
    ) {
        addMessage(FlashMessage.Level.ERROR, message, req);
    }

    static public void addSuccess(
        String message,
        HttpServletRequest req
    ) {
        addMessage(FlashMessage.Level.SUCCESS, message, req);
    }

    static public void addWarning(
        String message,
        HttpServletRequest req
    ) {
        addMessage(FlashMessage.Level.WARNING, message, req);
    }
}
