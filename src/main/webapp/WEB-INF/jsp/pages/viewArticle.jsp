<%@ page
  info="Display address weather and the eventual result"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Expects the following atributes:
  - article
 --%>

<c:import url = "../_header.jsp"/>

<section class="section">
  <article class="media box">
    <div class="media-content">
      <h3 class="title is-5">${ article.title }</h3>
      <p>Published: <strong>${ article.created }</strong></p>
      <div>
        ${article.body}
      </div>
    </div>
  </article>
</section>


<c:import url = "../_footer.jsp"/>
