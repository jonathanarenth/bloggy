<%@ page
  info="Display address weather and the eventual result"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Expects the following atributes:
  - articles: the list of article to display
  - articleCount
  - itemsPerPage
  - currentPage
 --%>

<c:import url = "../_header.jsp"/>

<section class="section">
  <h2 class="title is-4">Recent Articles</h2>
  <div class="columns is-multiline is-mobile">
    <c:forEach var="article" items="${articles}">
      <div class="column is-half">
        <article class="media box">
          <div class="media-content">
            <h3 class="title is-5">
              <a href="<c:url value="/article/view?id=${article.id}" />">
                ${ article.title }
              </a>
            </h3>
            <p>Published: <strong>${ article.created }</strong></p>
          </div>
        </article>
      </div>
    </c:forEach>
  </div>

<nav class="pagination is-rounded" role="navigation" aria-label="pagination">
  <c:set var="lastPage" value="${Math.ceil(articleCount / itemsPerPage)}" />

  <c:if test="${currentPage > 1}">
    <a class="pagination-previous"
      href="<c:url value="?page=${currentPage - 1}"/>"
      >Previous</a>
  </c:if>
  <c:if test="${currentPage < lastPage}">
    <a class="pagination-next"
      href="<c:url value="?page=${currentPage + 1}"/>"
      >Next page</a>
  </c:if>

  <ul class="pagination-list">
    <c:forEach var="page" begin="1" end="${lastPage}">
      <li>
        <a class="pagination-link ${page == currentPage ? 'is-current' : ''}"
          href="<c:url value="?page=${page}"/>"
          aria-label="Page <c:out value="page" />"
          aria-current="page">
          <c:out value="${page}" />
        </a>
      </li>
    </c:forEach>
  </ul>
</nav>
</section>


<c:import url = "../_footer.jsp"/>
