<%@ page
  info="Page Header"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="navbar" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="<c:url value="/" />">
      <img
        width="28"
        height="28"
        src="<c:url value="/static/favicon/android-chrome-192x192.png" />"
      />
    </a>

    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBasicExample" class="navbar-menu">
    <div class="navbar-start">
      <c:if test="${not empty sessionScope.user}">
        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link">
            Articles
          </a>
          <div class="navbar-dropdown">
            <a class="navbar-item"
              href="<c:url value="/article/create" />">
              Create an Article
            </a>
            <a class="navbar-item"
              href="<c:url value="/article/list" />">
              List
            </a>
          </div>
        </div>
      </c:if>
    </div>

    <div class="navbar-end">
      <div class="navbar-item">
        <c:if test="${not empty sessionScope.user}">
          ${sessionScope.user.username} (
            <a href="<c:url value="/logout" />">Log Out</a>
          )
        </c:if>

        <c:if test="${empty sessionScope.user}">
          <div class="buttons">
            <a class="button is-light"
              href="<c:url value="/login" />">
              LogIn
            </a>
          </div>
        </c:if>
      </div>
    </div>
  </div>
</nav>
