<%@ page
  info="Page Header"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bloggy - A poor man's blog</title>

    <link rel="apple-touch-icon"
      sizes="180x180"
      href="<c:url value="/static/favicon/apple-touch-icon.png" />">
    <link rel="icon"
      type="image/png"
      sizes="32x32"
      href="<c:url value="/static/favicon/favicon-32x32.png" />">
    <link rel="icon"
      type="image/png"
      sizes="16x16"
      href="<c:url value="/static/favicon/favicon-16x16.png" />">
    <link rel="manifest"
      href="<c:url value="/static/favicon/site.webmanifest" />">

    <link rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bulma@0.9.0/css/bulma.min.css">
  </head>
  <body>
    <header class="container has-text-right">
      <c:import url="/WEB-INF/jsp/_navbar.jsp"/>
    </header>

    <main class="container">
      <section class="flash-messages">
        <ul>
          <c:set var="class" />
          <c:forEach var="flash" items="${sessionScope.flashBag}">
            <c:choose>
              <c:when test="${ flash.level eq 'SUCCESS' }">
                <c:set var="class" value="is-success" />
              </c:when>
              <c:when test="${ flash.level eq 'WARNING' }">
                <c:set var="class" value="is-warning" />
              </c:when>
              <c:when test="${ flash.level eq 'ERROR' }">
                <c:set var="class" value="is-error" />
              </c:when>
              <c:otherwise>
                <c:set var="class" value="is-info" />
              </c:otherwise>
            </c:choose>

            <li class="notification <c:out value="class" />">${flash.message}</li>
          </c:forEach>

          <c:remove var="flashBag" scope="session"/>
        </ul>
      </section>
